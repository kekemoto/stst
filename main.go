package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	readFile(`./syntax`, func(line string) {
		fmt.Println(lexer(line))
	})
}

func lexer(line string) []string {
	// Go の文字列：https://qiita.com/seihmd/items/4a878e7fa340d7963fee
	var result []string
	var token []rune
	remove_delimiters := []rune{' '}
	remain_delimiters := []rune{'(', ')'}

	for _, char := range line {
		found := false

		for _, del := range remove_delimiters {
			if char == del {
				result = append(result, string(token))
				token = nil
				found = true
				break
			}
		}

		for _, del := range remain_delimiters {
			if char == del {
				token = append(token, del)
				result = append(result, string(token))
				token = nil
				found = true
				break
			}
		}

		if !found {
			token = append(token, char)
		}
	}

	return result
}

func readFile(path string, fn func(line string)) {
	// file pointer
	fp, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	// reading
	scanner := bufio.NewScanner(fp)

	for scanner.Scan() {
		fn(scanner.Text())
	}
}

func readFileToSlice(path string) []string {
	var lines []string
	readFile(path, func(line string) {
		lines = append(lines, line)
	})
	return lines
}
