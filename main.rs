#[macro_use]
extern crate lazy_static;

use std::collections::HashSet;
use std::collections::VecDeque;

lazy_static! {
    static ref REMOVE_DELIMITER: HashSet<char> = {
        vec![' '].into_iter().collect()
    };
}

fn main() {
    let mut lexer = Lexer::new("define_rule <- rule DEL ASSIGN (DEL formula)+ $EOL".to_owned());
    lexer.parseText();
    lexer.cut();

    println!("{:?}", lexer);
}

let remove_delimiter: HashSet<char> = vec![' '].into_iter().collect();
let delimiter: HashSet<char> = vec!['(', ')', '/', '?', '*', '+'];

#[derive(Debug)]
struct Lexer {
    text: VecDeque<char>,
    token: VecDeque<char>,
    tokens: VecDeque<String>,
}

impl Lexer {
    fn new(text: String) -> Self {
        Lexer {
            text: text.chars().collect(),
            token: VecDeque::new(),
            tokens: VecDeque::new(),
        }
    }

    fn pop(&mut self) -> Option<char> {
        self.text.pop_front()
    }

    fn push(&mut self, input: char) {
        self.token.push_back(input)
    }

    fn cut(&mut self) {
        if self.token.is_empty() {
            return;
        }
        self.tokens.push_back(self.token.iter().collect::<String>());
        self.token = VecDeque::new();
    }

    fn parseText(&mut self) {
        self.pop().map(|x| match x {
            ' ' => {
                self.cut();
                self.parseText();
            }
            '(' => {
                self.cut();
                self.push(x);
                self.cut();
                self.parseGroup();
            }
            _ => {
                self.push(x);
                self.parseText();
            }
        });
    }

    fn parseGroup(&mut self) {
        self.pop().map(|x| match x {
            ')' => {
                self.cut();
                self.push(x);
                self.cut();
                self.parseText();
            }
            '\\' => match self.pop() {
                None => return,
                Some(y) => {
                    self.push(y);
                    self.parseGroup();
                }
            },
            _ => {
                self.push(x);
                self.parseGroup();
            }
        });
    }
}
